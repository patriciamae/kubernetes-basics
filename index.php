<?php


// Sample data for 5 records
$sampleData = [
    [
        'id' => 1,
        'fullname' => 'John Doe',
        'email_address' => 'john.doe@example.com',
        'contact_number' => '+1234567890',
        'gender' => 'Male',
        'address' => '123 Main Street, Cityville'
    ],
    [
        'id' => 2,
        'fullname' => 'Jane Smith',
        'email_address' => 'jane.smith@example.com',
        'contact_number' => '+9876543210',
        'gender' => 'Female',
        'address' => '456 Oak Avenue, Townsville'
    ],
];


// Convert the data to JSON
$jsonResponse = json_encode($sampleData, JSON_PRETTY_PRINT);


// Set the Content-Type header to application/json
header('Content-Type: application/json');


// Output the JSON response
echo $jsonResponse;
